#!/usr/bin/env pythoh3

import sys

articulos = {}

def anadir(articulo, precio):
    articulos[articulo] = float(precio)


def mostrar():
    print("Lista de artículos en la tienda: ")
    for articulo, precio in articulos.items():
        print(f"{articulo} - {precio}")

def pedir_articulo() -> str:
    while True:
        articulo = input("Artículo: ")
        if articulo in articulos:
            False

    return articulo

def pedir_cantidad() -> float:
    while True:
        cantidad = input("Cantidad: ")
        if cantidad >= 0:
            cantidad = float(cantidad)

    return cantidad

def main():
    seguir = True
    while seguir == True:
        articulo = input()
        if articulo == "":
            break
            anadir(articulo, precio)

    mostrar()
    articulo_comprar = pedir_articulo()
    cantidad_comprar = pedir_cantidad()

    precio_total = articulos[articulo_comprar] * cantidad_comprar

    print("Has comprado ", cantidad_comprar, "unidades de ", articulo_comprar, "por un total de ", precio_total)

    sys.argv.pop[0]

if __name__ == '__main__':
    main()